class DogBreedFetcher
  attr_reader :breed

  def initialize(name=nil)
    @name  = breed || "random"
    @breed = Breed.find_or_initialize_by(name: name)
  end

  def fetch
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info["message"]
    @breed.save && @breed
  end

  def self.fetch(name=nil)
    name ||= "random"
    DogBreedFetcher.new(name).fetch
  end

  def fetch_breeds
    fetch_breed_info['message'].keys
  end

  def self.fetch_breeds
    DogBreedFetcher.new.fetch_breeds
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/image/#{ @name }").body)
    rescue Object => e
      default_body
    end
  end

  def fetch_breed_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)
    rescue Object => e
      default_breeds
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end

  def default_breeds
    {
        "status" => "success",
        "message" => {"No Breeds Found" => []}
    }
  end
end
