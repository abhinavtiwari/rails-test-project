class BreedsController < ApplicationController

  def index
    @breeds = DogBreedFetcher.fetch_breeds
  end

  def show
    @breed = DogBreedFetcher.fetch(params[:id])
  end

end
